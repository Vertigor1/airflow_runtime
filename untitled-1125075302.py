from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator
from airflow import DAG
from airflow.utils.dates import days_ago


args = {
    "project_id": "untitled-1125075302",
}

dag = DAG(
    "untitled-1125075302",
    default_args=args,
    schedule_interval="@once",
    start_date=days_ago(1),
    description="Created with Elyra 3.4.0.dev0 pipeline editor using `untitled.pipeline`.",
    is_paused_upon_creation=False,
)


# Operator source: hello.ipynb
op_f6db525f_8f23_4acd_b31e_5c1d059302c1 = KubernetesPodOperator(
    name="hello",
    namespace="airflow",
    image="continuumio/anaconda3:2020.07",
    cmds=["sh", "-c"],
    arguments=[
        "mkdir -p ./jupyter-work-dir/ && cd ./jupyter-work-dir/ && curl -H 'Cache-Control: no-cache' -L https://raw.githubusercontent.com/elyra-ai/elyra/master/elyra/airflow/bootstrapper.py --output bootstrapper.py && curl -H 'Cache-Control: no-cache' -L https://raw.githubusercontent.com/elyra-ai/elyra/master/etc/generic/requirements-elyra.txt --output requirements-elyra.txt && python3 -m pip install packaging && python3 -m pip freeze > requirements-current.txt && python3 bootstrapper.py --cos-endpoint http://10.135.12.248:9000 --cos-bucket test --cos-directory 'untitled-1125075302' --cos-dependencies-archive 'hello-f6db525f-8f23-4acd-b31e-5c1d059302c1.tar.gz' --file 'hello.ipynb' "
    ],
    task_id="hello",
    env_vars={
        "ELYRA_RUNTIME_ENV": "airflow",
        "AWS_ACCESS_KEY_ID": "minioadmin",
        "AWS_SECRET_ACCESS_KEY": "minioadmin",
        "ELYRA_ENABLE_PIPELINE_INFO": "True",
        "ELYRA_RUN_NAME": "untitled-1125075302-{{ ts_nodash }}",
    },
    in_cluster=True,
    config_file="None",
    dag=dag,
)
