from airflow.operators.bash_operator import BashOperator
from airflow import DAG
from airflow.utils.dates import days_ago


args = {
    "project_id": "untitled3-1224023526",
}

dag = DAG(
    "untitled3-1224023526",
    default_args=args,
    schedule_interval="@once",
    start_date=days_ago(1),
    description="Created with Elyra 3.5.0.dev0 pipeline editor using `untitled3.pipeline`.",
    is_paused_upon_creation=False,
)


# Operator source: {'catalog_type': 'elyra-airflow-examples-catalog', 'component_ref': {'component-id': 'bash_operator.py'}}
op_cc55f752_b916_42f9_b7a3_3e9a4158ab61 = BashOperator(
    task_id="BashOperator",
    bash_command="echo 'hello'",
    xcom_push=True,
    env={},
    output_encoding="utf-8",
    dag=dag,
)
